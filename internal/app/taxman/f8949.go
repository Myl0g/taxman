package taxman

import "time"

type Form8949Entry struct {
	Description    string
	DateAcquired   time.Time
	DateSold       time.Time
	Proceeds       float64
	Cost           float64
	AdjustmentCode rune
	AdjustmentAmt  float64
}

type Form8949 struct {
	ShortTermBox          int
	ShortTermTransactions []Form8949Entry
	LongTermBox           int
	LongTermTransactions  []Form8949Entry
}
