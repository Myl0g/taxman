package taxman

import "fmt"

type Form1099Div struct {
	UserProvidedName string
	Line1a           float64
	Line1b           float64
	Line2a           float64
	Line2b           float64
	Line2c           float64
	Line2d           float64
	Line2e           float64
	Line2f           float64
	Line3            float64
	Line4            float64
	Line5            float64
	Line6            float64
	Line7            float64
	Line8            string
	Line9            float64
	Line10           float64
	Line12           float64
	Line13           float64
}

type Form1099Int struct {
	UserProvidedName string
	Line1            float64
	Line2            float64
	Line3            float64
	Line4            float64
	Line5            float64
	Line6            float64
	Line7            string
	Line8            float64
	Line9            float64
	Line10           float64
	Line11           float64
	Line12           float64
	Line13           float64
	Line14           string
}

type Form1099Misc struct {
	UserProvidedName string
	Line2            float64
	Line3            float64
	Line4            float64
	Line8            float64
}

type Form1099B struct {
	UserProvidedName string
	Line8            float64
	Line9            float64
	Line10           float64
	Line11           float64
}

type Form1099 struct {
	UserProvidedName string
	Div              Form1099Div
	Int              Form1099Int
	Misc             Form1099Misc
	B                Form1099B
}

func (f *Form1099Div) GetFormProviderName() string {
	return f.UserProvidedName
}

func (f *Form1099Div) SetFormProviderName(s string) {
	f.UserProvidedName = s
}

func (f *Form1099Int) GetFormProviderName() string {
	return f.UserProvidedName
}

func (f *Form1099Int) SetFormProviderName(s string) {
	f.UserProvidedName = s
}

func (f *Form1099Misc) GetFormProviderName() string {
	return f.UserProvidedName
}

func (f *Form1099Misc) SetFormProviderName(s string) {
	f.UserProvidedName = s
}

func (f *Form1099B) GetFormProviderName() string {
	return f.UserProvidedName
}

func (f *Form1099B) SetFormProviderName(s string) {
	f.UserProvidedName = s
}

func (f *Form1099) GetFormProviderName() string {
	return f.UserProvidedName
}

func (f *Form1099) SetFormProviderName(s string) {
	f.UserProvidedName = s
}

func (f *Form1099) WalkthroughForm() {
	fmt.Println("This walkthrough assumes you have received a consolidated 1099. If you see sections which you do not have answers for, do not enter any values - just press Enter and continue.")
	fmt.Println("Beginning 1099-DIV section.")
	f.Div.UserProvidedName = f.UserProvidedName + "-DIV"
	f.Div.WalkthroughForm()
	fmt.Println("Beginning 1099-INT section.")
	f.Int.UserProvidedName = f.UserProvidedName + "-INT"
	f.Int.WalkthroughForm()
	fmt.Println("Beginning 1099-MISC section.")
	f.Misc.UserProvidedName = f.UserProvidedName + "-MISC"
	f.Misc.WalkthroughForm()
	fmt.Println("Beginning 1099-B section.")
	f.B.UserProvidedName = f.UserProvidedName + "-B"
	f.B.WalkthroughForm()
}

func (f *Form1099Div) WalkthroughForm() {
	commonWalkthrough(f, commonQuestionsBuilder(f))
}

func (f *Form1099Int) WalkthroughForm() {
	commonWalkthrough(f, commonQuestionsBuilder(f))
}

func (f *Form1099Misc) WalkthroughForm() {
	commonWalkthrough(f, commonQuestionsBuilder(f))
}

func (f *Form1099B) WalkthroughForm() {
	commonWalkthrough(f, commonQuestionsBuilder(f))
}
