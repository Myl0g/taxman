package taxman

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

const COMMON_FLOAT_LEN = 30

func askUserCommon(question string, n int) []rune {
	var char rune
	var err error
	s := make([]rune, n)

	fmt.Printf(question + ": ")

	reader := bufio.NewReader(os.Stdin)

	for i := 0; i < n; i++ {
		char, _, err = reader.ReadRune()
		if err != nil {
			log.Fatal(err)
		}
		s[i] = char
	}

	return s
}

func AskUserForFloat(question string, n int) float64 {
	var answer float64
	var err error
	s := askUserCommon(question+" (an integer or decimal value)", n)

	if len(s) == 0 {
		return 0
	}

	answer, err = strconv.ParseFloat(string(s), 64)
	if err != nil {
		log.Fatal(err)
	}

	return answer
}

func AskUserForInt(question string, n int) int {
	var answer int64
	var err error
	s := askUserCommon(question+" (an integer, no decimal value)", n)

	if len(s) == 0 {
		return 0
	}

	answer, err = strconv.ParseInt(string(s), 10, 0)
	if err != nil {
		log.Fatal(err)
	}

	return int(answer)
}

func AskUserForBool(question string) bool {
	s := string(askUserCommon(question+" (Answer 'Yes'/'Y' case insensitive or something else for No)", 3))
	fixed := strings.ToLower(s)
	return fixed == "y" || fixed == "yes"
}

func AskUserForStr(question string, n int) string {
	s := askUserCommon(question, n)
	return string(s)
}

func AskUserForMultipleChoice(line string, options ...string) int {
	return -1
}
