package taxman

import (
	"reflect"
	"strings"
)

type TaxForm interface {
	GetFormProviderName() string
	SetFormProviderName(s string)
	WalkthroughForm()
}

type ResultantTaxForm interface {
	GeneratePDF(outFilePath string)
	GetFormProviderName() string
	SetFormProviderName(s string)
	WalkthroughForm()
}

// Splits each struct name with a space separating Line and the number.
func commonQuestionsBuilder(f TaxForm) []string {
	result := make([]string, 0)

	v := reflect.ValueOf(f).Elem()
	for i := 0; i < v.NumField(); i++ {
		f := v.Field(i)
		n := f.Type().Field(i).Name
		lineNumber := strings.Split(n, "Line")[1]

		result = append(result, "Enter "+lineNumber+" (or just press Enter if zero/blank)")
	}

	return result
}

func commonWalkthrough(f TaxForm, questions []string) {
	v := reflect.ValueOf(f).Elem()
	for i := 0; i < v.NumField(); i++ {
		f := v.Field(i)
		n := f.Type().Field(i).Name

		switch f.Kind() {
		case reflect.Float32:
			fallthrough
		case reflect.Float64:
			f.Set(reflect.ValueOf(AskUserForFloat(questions[i], COMMON_FLOAT_LEN)))
		case reflect.Int64:
			fallthrough
		case reflect.Int32:
			fallthrough
		case reflect.Int16:
			fallthrough
		case reflect.Int:
			f.Set(reflect.ValueOf(AskUserForMultipleChoice(n, strings.Split(questions[i], ";")...)))
		case reflect.Bool:
			f.Set(reflect.ValueOf(AskUserForBool(questions[i])))
		}
	}
}
