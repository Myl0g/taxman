package taxman

import "fmt"

type FormW2 struct {
	UserProvidedName string
	Line1            float64
	Line2            float64
	Line3            float64
	Line4            float64
	Line5            float64
	Line6            float64
	Line7            float64
	Line8            float64
	Line10           float64
	Line11           float64
	Line13           int
	Line16           float64
	Line17           float64
	Line18           float64
	Line19           float64
}

// func (f *FormW2) WalkthroughForm() {
// 	f.Line1 = AskUserForFloat("Enter Line 1 (Wages, tips, other comp)", COMMON_FLOAT_LEN)
// 	f.Line2 = AskUserForFloat("Enter Line 2 (or just press Enter if zero/blank)", COMMON_FLOAT_LEN)
// 	f.Line3 = AskUserForFloat("Enter Line 3 (or just press Enter if zero/blank)", COMMON_FLOAT_LEN)
// 	f.Line4 = AskUserForFloat("Enter Line 4 (or just press Enter if zero/blank)", COMMON_FLOAT_LEN)
// 	f.Line5 = AskUserForFloat("Enter Line 5 (or just press Enter if zero/blank)", COMMON_FLOAT_LEN)
// 	f.Line6 = AskUserForFloat("Enter Line 6 (or just press Enter if zero/blank)", COMMON_FLOAT_LEN)
// 	f.Line7 = AskUserForFloat("Enter Line 7 (or just press Enter if zero/blank)", COMMON_FLOAT_LEN)
// 	f.Line8 = AskUserForFloat("Enter Line 8 (or just press Enter if zero/blank)", COMMON_FLOAT_LEN)
// 	f.Line10 = AskUserForFloat("Enter Line 10 (or just press Enter if zero/blank)", COMMON_FLOAT_LEN)
// 	f.Line11 = AskUserForFloat("Enter Line 11 (or just press Enter if zero/blank)", COMMON_FLOAT_LEN)
// 	f.Line13 = AskUserForMultipleChoice("Line 13", "Statutory employee", "Retirement plan", "Third party sick pay")
// 	f.Line16 = AskUserForFloat("Enter Line 16 (or just press Enter if zero/blank)", COMMON_FLOAT_LEN)
// 	f.Line17 = AskUserForFloat("Enter Line 17 (or just press Enter if zero/blank)", COMMON_FLOAT_LEN)
// 	f.Line18 = AskUserForFloat("Enter Line 18 (or just press Enter if zero/blank)", COMMON_FLOAT_LEN)
// 	f.Line19 = AskUserForFloat("Enter Line 19 (or just press Enter if zero/blank)", COMMON_FLOAT_LEN)
// }

func (f *FormW2) GetFormProviderName() string {
	return f.UserProvidedName
}

func (f *FormW2) SetFormProviderName(s string) {
	f.UserProvidedName = s
}

func (f *FormW2) WalkthroughForm() {
	questions := []string{"Enter Line 1 (Wages, tips, other comp)"}
	var i int
	for i = 2; i <= 11; i++ {
		if i == 9 {
			i++
		}
		questions = append(questions, "Enter Line "+fmt.Sprintf("%d", i)+" (or just press Enter if zero/blank)")
	}
	questions = append(questions, "Statutory employee;Retirement plan;Third party sick pay")

	for i = 16; i <= 19; i++ {
		questions = append(questions, "Enter Line "+fmt.Sprintf("%d", i)+" (or just press Enter if zero/blank)")
	}

	commonWalkthrough(f, questions)
}
