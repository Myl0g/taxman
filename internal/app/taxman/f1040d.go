package taxman

type Form1040d struct {
	UserProvidedName        string
	DidDisposeQualifiedFund bool
	Line1ad                 float64
	Line1ae                 float64
	Line1bd                 float64
	Line1be                 float64
	Line1bg                 float64
	Line2d                  float64
	Line2e                  float64
	Line2g                  float64
	Line3d                  float64
	Line3e                  float64
	Line3g                  float64
	Line6                   float64
	Line8ad                 float64
	Line8ae                 float64
	Line8bd                 float64
	Line8be                 float64
	Line8bg                 float64
	Line9d                  float64
	Line9e                  float64
	Line9g                  float64
	Line10d                 float64
	Line10e                 float64
	Line10g                 float64
	Line13                  float64
	Line14                  float64
}

func (f *Form1040d) WalkthroughForm() {

}
func (f *Form1040d) GetFormProviderName() string {
	return f.UserProvidedName
}
func (f *Form1040d) SetFormProviderName(s string) {
	f.UserProvidedName = s
}

func (f *Form1040d) NetShortTermCapitalGain() float64 {
	return 0.0
}
func (f *Form1040d) NetLongTermCapitalGain() float64 {
	return 0.0
}
func (f *Form1040d) FinalCapitalGain() float64 {
	first := f.NetLongTermCapitalGain() + f.NetShortTermCapitalGain()
	if first >= 0 {
		return first
	}

	if (first * -1) < 3000 {
		return first
	}

	return 3000
}
