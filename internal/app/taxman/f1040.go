package taxman

import (
	"fmt"
	"log"
)

type Form1040 struct {
	UserProvidedName       string
	FilingStatus           int
	FirstNameMiddleInitial string
	LastName               string
	Ssn                    string
	HomeAddress            string
	AptNo                  string
	City                   string
	State                  string
	Zip                    string
	DigitalAssets          bool
	StandardDeduction      int
	Line1b                 float64
	Line1c                 float64
	Line1d                 float64
	Line1h                 float64
	Line1i                 float64
	Line2a                 float64
	Line2b                 float64
	Line3a                 float64
	Line3b                 float64
	Line4a                 float64
	Line4b                 float64
	Line5a                 float64
	Line5b                 float64
	Line6a                 float64
	Line6b                 float64
	Line6c                 bool
	Line7                  float64
	Line7extra             bool
	Line16extra            int
	Line16extra2           TaxForm
	Line35aextra           bool
	Line35b                string
	Line35c                int
	Line35d                string
	Occupation             string
	Email                  string
	PhoneNumber            string
}

func (f *Form1040) WalkthroughForm() {

}

func (f *Form1040) GeneratePDF(outFilePath string) {
	log.Fatal("This function should not be called. f1040 has requisite forms; call GeneratePDFWithRequisites instead.")
}

// Matches Field names in the IRS-provided PDFs with the fields in the Form struct.
// You can obtain field names by running `qpdf form.pdf --json | jq '.acroform.fields'` on
// any accessible format of the IRS form.
func (f *Form1040) GeneratePDFWithRequisites(outFilePath string, w2s []FormW2, f1099s []Form1099, sd Form1040d) {
	options := make([]fdfOption, 0)

	// First let's fill in the information directly entered by the user.

	switch f.FilingStatus {
	case 1:
		options = append(options, fdfOption{Key: "topmostSubform[0].Page1[0].c1_01[0]", Value: "On"})
	case 2:
		options = append(options, fdfOption{Key: "topmostSubform[0].Page1[0].c1_01[1]", Value: "On"})
	case 3:
		options = append(options, fdfOption{Key: "topmostSubform[0].Page1[0].c1_01[2]", Value: "On"})
	case 4:
		options = append(options, fdfOption{Key: "topmostSubform[0].Page1[0].c1_01[3]", Value: "On"})
	case 5:
		options = append(options, fdfOption{Key: "topmostSubform[0].Page1[0].c1_01[4]", Value: "On"})
	}

	options = append(options,
		fdfOption{Key: "topmostSubform[0].Page1[0].f1_02[0]", Value: f.FirstNameMiddleInitial},
		fdfOption{Key: "topmostSubform[0].Page1[0].f1_03[0]", Value: f.LastName},
		fdfOption{Key: "topmostSubform[0].Page1[0].YourSocial[0].f1_04[0]", Value: f.Ssn},
		fdfOption{Key: "topmostSubform[0].Page1[0].Address[0].f1_08[0]", Value: f.HomeAddress},
		fdfOption{Key: "topmostSubform[0].Page1[0].Address[0].f1_09[0]", Value: f.AptNo},
		fdfOption{Key: "topmostSubform[0].Page1[0].Address[0].f1_10[0]", Value: f.City},
		fdfOption{Key: "topmostSubform[0].Page1[0].Address[0].f1_11[0]", Value: f.State},
		fdfOption{Key: "topmostSubform[0].Page1[0].Address[0].f1_12[0]", Value: f.Zip},
	)

	if f.DigitalAssets {
		options = append(options, fdfOption{Key: "topmostSubform[0].Page1[0].c1_04[0]", Value: "On"})
	}

	switch f.StandardDeduction {
	case 1:
		options = append(options, fdfOption{Key: "topmostSubform[0].Page1[0].c1_05[0]", Value: "On"})
	case 2:
		options = append(options, fdfOption{Key: "topmostSubform[0].Page1[0].c1_06[0]", Value: "On"})
	case 3:
		options = append(options, fdfOption{Key: "topmostSubform[0].Page1[0].c1_07[0]", Value: "On"})
	}

	// Now let's calculate the rest of the fields we need to fill in.

	line1a := 0.0
	line1z := 0.0
	line2a := 0.0
	line2b := 0.0
	line3a := 0.0
	line3b := 0.0
	line7 := 0.0
	line9 := 0.0
	line11 := 0.0
	line12 := 0.0
	line14 := 0.0
	line15 := 0.0
	line16 := 0.0
	line18 := 0.0
	line22 := 0.0
	line24 := 0.0
	line25a := 0.0
	line25b := 0.0
	line25d := 0.0
	line33 := 0.0
	line34 := 0.0
	line35a := 0.0
	line37 := 0.0

	for _, form := range w2s {
		line1a += form.Line1
		line25a += form.Line2
	}

	for _, form := range f1099s {
		line2a += form.Int.Line8
		line2b += form.Int.Line1
		line3a += form.Div.Line1b
		line3b += form.Div.Line1a
		line25b += 0 // TODO
	}

	line7 = sd.FinalCapitalGain()
	line1z = line1a
	line9 = line1z + line2b + line3b + line7
	line11 = line9
	switch f.StandardDeduction {
	case 0:
		line12 = 12950.0
	case 1:
		tmp := line1z
		if line1z > 750.0 {
			tmp += 400.0
		}
		if tmp < 12950.0 {
			line12 = tmp
		} else {
			line12 = 12950.0
		}
	}

	line14 = line12
	line15 = line11 - line14
	line16 = f.Tax()
	line18 = line16
	line22 = line18
	line24 = line22
	line25d = line25a + line25b
	line33 = line25d

	if line33-line24 >= 0 {
		line34 = line33 - line24
		line35a = line34
	} else {
		line37 = line24 - line33
	}

	options = append(options,
		fdfOption{Key: "topmostSubform[0].Page1[0].f1_28[0]", Value: fmt.Sprintf("%f", line1a)},
		fdfOption{Key: "topmostSubform[0].Page1[0].f1_37[0]", Value: fmt.Sprintf("%f", line1z)},
		fdfOption{Key: "topmostSubform[0].Page1[0].f1_38[0]", Value: fmt.Sprintf("%f", line2a)},
		fdfOption{Key: "topmostSubform[0].Page1[0].f1_39[0]", Value: fmt.Sprintf("%f", line2b)},
		fdfOption{Key: "topmostSubform[0].Page1[0].f1_40[0]", Value: fmt.Sprintf("%f", line3a)},
		fdfOption{Key: "topmostSubform[0].Page1[0].f1_41[0]", Value: fmt.Sprintf("%f", line3b)},
		fdfOption{Key: "topmostSubform[0].Page1[0].Lines4a-11_ReadOrder[0].f1_48[0]", Value: fmt.Sprintf("%f", line7)},
		fdfOption{Key: "topmostSubform[0].Page1[0].Lines4a-11_ReadOrder[0].f1_50[0]", Value: fmt.Sprintf("%f", line9)},
		fdfOption{Key: "topmostSubform[0].Page1[0].Lines4a-11_ReadOrder[0].f1_52[0]", Value: fmt.Sprintf("%f", line11)},
		fdfOption{Key: "topmostSubform[0].Page1[0].StandardDeductionBubble[0].f1_53[0]", Value: fmt.Sprintf("%f", line12)},
		fdfOption{Key: "topmostSubform[0].Page1[0].f1_55[0]", Value: fmt.Sprintf("%f", line14)},
		fdfOption{Key: "topmostSubform[0].Page1[0].f1_56[0]", Value: fmt.Sprintf("%f", line15)},
		fdfOption{Key: "topmostSubform[0].Page2[0].f2_02[0]", Value: fmt.Sprintf("%f", line16)},
		fdfOption{Key: "topmostSubform[0].Page2[0].f2_04[0]", Value: fmt.Sprintf("%f", line18)},
		fdfOption{Key: "topmostSubform[0].Page2[0].f2_08[0]", Value: fmt.Sprintf("%f", line22)},
		fdfOption{Key: "topmostSubform[0].Page2[0].f2_10[0]", Value: fmt.Sprintf("%f", line24)},
		fdfOption{Key: "topmostSubform[0].Page2[0].f2_11[0]", Value: fmt.Sprintf("%f", line25a)},
		fdfOption{Key: "topmostSubform[0].Page2[0].f2_12[0]", Value: fmt.Sprintf("%f", line25b)},
		fdfOption{Key: "topmostSubform[0].Page2[0].f2_14[0]", Value: fmt.Sprintf("%f", line25d)},
		fdfOption{Key: "topmostSubform[0].Page2[0].f2_22[0]", Value: fmt.Sprintf("%f", line33)},
		fdfOption{Key: "topmostSubform[0].Page2[0].f2_23[0]", Value: fmt.Sprintf("%f", line34)},
		fdfOption{Key: "topmostSubform[0].Page2[0].f2_24[0]", Value: fmt.Sprintf("%f", line35a)},
		fdfOption{Key: "topmostSubform[0].Page2[0].f2_24[0]", Value: f.Line35b},
		fdfOption{Key: "topmostSubform[0].Page2[0].AccountNo[0].f2_26[0]", Value: f.Line35d},
		fdfOption{Key: "topmostSubform[0].Page2[0].f2_28[0]", Value: fmt.Sprintf("%f", line37)},
	)

	switch f.Line35c {
	case 1:
		options = append(options, fdfOption{Key: "topmostSubform[0].Page2[0].c2_05[0]", Value: "On"})
	case 2:
		options = append(options, fdfOption{Key: "topmostSubform[0].Page2[0].c2_05[1]", Value: "On"})
	}

	fillPdf(options, outFilePath)
}

func (f *Form1040) GetFormProviderName() string {
	return f.UserProvidedName
}

func (f *Form1040) SetFormProviderName(s string) {
	f.UserProvidedName = s
}
func (f *Form1040) Tax() float64 {
	return 0.0
}
