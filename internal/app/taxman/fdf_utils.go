package taxman

import (
	"errors"
	"os"
	"os/exec"
	"regexp"
)

type fdfOption struct {
	Key   string
	Value string // if checkbox : "On" or "Off" values only
}

func fillPdf(options []fdfOption, formName string) error {
	re := regexp.MustCompile(`f(\d{4})([a-g]*)`)
	if !re.Match([]byte(formName)) {
		return errors.New("formName did not match regex (input sanitization)")
	}

	fdf := `%FDF-1.2
1 0 obj<</FDF<< /Fields[`

	for _, option := range options {
		fdf = fdf + "\n" + "<< /T (" + option.Key + ") /V (" + option.Value + ") >>"
	}

	fdf = fdf + `] >> >>
endobj
trailer
<</Root 1 0 R>>
%%EOF
`

	os.WriteFile(formName+".fdf", []byte(fdf), 0600)
	_, err := exec.Command("pdftk", "unfilled/"+formName+".pdf", "fill_form", formName+".fdf", "output", formName+"-filled.pdf").Output()

	return err
}
