package main

import (
	"encoding/json"
	"log"
	"os"

	"github.com/myl0g/taxman/internal/app/taxman"
)

func generate() {
	var prefilledTaxes []taxman.TaxForm

	taxFile := os.Args[2]
	taxContents, err := os.ReadFile(taxFile)
	if err != nil {
		log.Fatal(err)
	}

	err = json.Unmarshal(taxContents, &prefilledTaxes)
	if err != nil {
		log.Fatal(err)
	}

	
}
