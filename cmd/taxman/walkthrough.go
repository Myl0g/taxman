package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	"github.com/myl0g/taxman/internal/app/taxman"
)

func walkthrough() {
	var forms []taxman.TaxForm
	var n int

	if taxman.AskUserForBool("Did you receive any W-2 forms?") {
		n = taxman.AskUserForInt("How many?", 2)
		fw2s := make([]taxman.TaxForm, n)

		for i := 0; i < n; i++ {
			fw2s[i] = &taxman.FormW2{}
			fmt.Printf("Now processing W-2 #%d of %d\n", i+1, n)
			fw2s[i].SetFormProviderName(taxman.AskUserForStr("What is the name of the company who provided you with this form?", 30))
			fw2s[i].WalkthroughForm()
		}

		forms = append(forms, fw2s...)
	}

	if taxman.AskUserForBool("Did you receive any 1099 forms?") {
		n = taxman.AskUserForInt("How many?", 2)
		f1099s := make([]taxman.TaxForm, n)

		for i := 0; i < n; i++ {
			f1099s[i] = &taxman.Form1099{}
			fmt.Printf("Now processing 1099 #%d of %d\n", i+1, n)
			f1099s[i].SetFormProviderName(taxman.AskUserForStr("What is the name of the company who provided you with this form?", 30))
			f1099s[i].WalkthroughForm()
		}

		forms = append(forms, f1099s...)
	}

	fmt.Println("Please refer to IRS Form 1040 to answer the remaining questions. You are nearly done.")
	f1040 := &taxman.Form1040{}
	f1040.WalkthroughForm()
	forms = append([]taxman.TaxForm{f1040}, forms...)

	fmt.Println("Your data entry is complete. We are saving your data to a file called 'taxes.json' in the current directory...")
	jsonSave, err := json.Marshal(forms)
	if err != nil {
		log.Fatal(err)
	}
	if err := os.WriteFile("taxes.json", jsonSave, 0666); err != nil {
		fmt.Println("We encountered an error and the 'taxes.json' file could not be created.\nTo prevent you from losing your tax data, we shall print it to the screen now.")
		fmt.Println(jsonSave)
		log.Fatal(err)
	}

	fmt.Println("All your necessary tax data has been saved to 'taxes.json'. Run 'taxman generate taxes.json' to fill your tax forms.")
}
