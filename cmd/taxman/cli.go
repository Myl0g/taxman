package main

import (
	"fmt"
	"os"
)

func main() {
	option := os.Args[1]
	switch option {
	case "walkthrough":
		walkthrough()
	case "generate":
		generate()
	default:
		fmt.Println("Unrecognized option provided as 1st argument. Try: walkthrough; generate")
		os.Exit(1)
	}

}
